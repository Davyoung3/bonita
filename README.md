# bonita_challenge



## Getting started

For this test, is on minikube environment test

# Runtime DataBase

## Create namespace with bonita name
kubectl create ns bonita

and for setting the namespace by default use this command

$ kubectl config set-context --current --namespace=bonita

## For this test runtimem, the database must be deployed first
PostgreSQL database, deployment as StatefulSet kind

## Postgres service and secret deployment
$ kubectl apply -f bonita-postgres-svc -n bonita

$ kubectl apply -f bonita-postgres-secret -n bonita

## PV and PVC deployment for data persistence
$ kubectl apply -f bonita-postgres-pv -n bonita

## Then proced to the deployment of PostgreSQL database
$ kubectl apply -f bonita-postgres-dpl -n bonita

# Runtime Application
$ kubectl apply -f bonita-apps-svc -n bonita

$ kubectl apply -f bonita-postgres-cfm -n bonita

$ kubectl apply -f bonita-postgres-dpl -n bonita